import { GameCard } from './GameCard';
import { newImage } from './util';

const stackImage = newImage('stack');

export class Stack {
   public cards: GameCard[] = [];
   private x: number;
   private y: number;

   public constructor(
      x: number, y: number,
      private offset = 0, private reveal = true
   ) {
      this.x = x * 160 + 20;
      this.y = y * 210 + 20;
   }

   public drawBase(ctx: CanvasRenderingContext2D): void {
      ctx.globalAlpha = 0.5;
      ctx.drawImage(stackImage, this.x, this.y);
      ctx.globalAlpha = 1;
   }

   public draw(ctx: CanvasRenderingContext2D): void {
      const length = this.cards.length;
      for (let i = 0; i < length; ++i) {
         this.cards[length - 1 - i].draw(
            ctx,
            this.x,
            this.y + i * this.offset
         );
      }
   }

   public moveCards(count: number, to: Stack): void {
      to.cards = this.cards.splice(0, count).concat(to.cards);
      if (this.reveal && this.cards.length > 0) {
         this.cards[0].visible = true;
      }
   }

   public clone(): Stack {
      const stack = new Stack(0, 0, this.offset, this.reveal);
      stack.x = this.x;
      stack.y = this.y;
      stack.cards = this.cards.map(card => card.clone());
      return stack;
   }
}
