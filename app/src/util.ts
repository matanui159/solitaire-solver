export function newImage(name: string): HTMLImageElement {
   const image = new Image();
   image.onerror = console.error;
   image.src = `cards/${name}.png`;
   return image;
}
