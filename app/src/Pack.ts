import { GameCard } from "./GameCard";
import { cards } from "./Card";

export class Pack {
   public target = new EventTarget();

   public get empty(): boolean {
      return this.cards.length === 0;
   }

   private cards: GameCard[];

   public constructor() {
      let loadCount = 0;
      this.cards = cards.map(card => {
         const gcard = new GameCard(card);
         gcard.image.onload = () => {
            if (++loadCount === cards.length) {
               this.target.dispatchEvent(new Event('load'));
            }
         };
         gcard.visible = false;
         return gcard;
      });
   }

   public randomCard(): GameCard {
      const index = Math.floor(Math.random() * this.cards.length);
      const card = this.cards[index];
      this.cards.splice(index, 1);
      return card;
   }
}
