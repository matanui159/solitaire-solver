import { Stack } from './Stack';
import { Pack } from './Pack';

const offset = 30;

export class Board {
   public down = new Stack(0, 0, 0, false);
   public up = new Stack(1, 0);
   public top: { [suit: string]: Stack } = {
      C: new Stack(3, 0),
      D: new Stack(4, 0),
      S: new Stack(5, 0),
      H: new Stack(6, 0)
   };
   public bottom = [
      new Stack(0, 1, offset),
      new Stack(1, 1, offset),
      new Stack(2, 1, offset),
      new Stack(3, 1, offset),
      new Stack(4, 1, offset),
      new Stack(5, 1, offset),
      new Stack(6, 1, offset)
   ];
   public pack: Pack;

   public constructor() {
      const pack = new Pack();
      for (let i = 0; i < this.bottom.length; ++i) {
         for (let j = 0; j <= i; ++j) {
            this.bottom[i].cards.unshift(pack.randomCard());
         }
         this.bottom[i].cards[0].visible = true;
      }

      const card = pack.randomCard();
      card.visible = true;
      this.up.cards.unshift(card);
      while (!pack.empty) {
         this.down.cards.unshift(pack.randomCard());
      }

      this.pack = pack;
   }

   public draw(ctx: CanvasRenderingContext2D): void {
      this.down.drawBase(ctx);
      this.up.drawBase(ctx);
      for (const suit in this.top) {
         this.top[suit].drawBase(ctx);
      }
      for (const stack of this.bottom) {
         stack.drawBase(ctx);
      }

      this.down.draw(ctx);
      this.up.draw(ctx);
      for (const stack of this.bottom) {
         stack.draw(ctx);
      }
      for (const suit in this.top) {
         this.top[suit].draw(ctx);
      }
   }

   public isFinished(): boolean {
      for (const suit in this.top) {
         if (this.top[suit].cards.length < 13) {
            return false;
         }
      }
      return true;
   }

   public clone(): Board {
      const board = new Board();
      board.down = this.down.clone();
      board.up = this.up.clone();
      for (const suit in this.top) {
         board.top[suit] = this.top[suit].clone();
      }
      for (let i = 0; i < this.bottom.length; ++i) {
         board.bottom[i] = this.bottom[i].clone();
      }
      return board;
   }
}
