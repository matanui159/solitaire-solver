export enum Suit {
   clubs = 'C',
   diamonds = 'D',
   spades = 'S',
   hearts = 'H'
}

export enum Face {
   ace = 1,
   n2,
   n3,
   n4,
   n5,
   n6,
   n7,
   n8,
   n9,
   n10,
   jack,
   queen,
   king
}

export interface Card {
   readonly suit: Suit;
   readonly face: Face;
}

export const cards: Card[] = [];
for (const suit in Suit) {
   for (let face = Face.ace; face <= Face.king; ++face) {
      cards.push({
         suit: Suit[suit] as Suit,
         face
      });
   }
}
