import { Board } from './Board';
import { Solver } from './Solver';

const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d')!;

let board: Board;
let solver: Solver;
do {
   board = new Board();
   solver = new Solver(board);
} while (!solver.isSolvable());

function main() {
   setInterval(() => {
      for (let i = 0; i < 1; ++i) {
         solver.step();
      }
   }, 100);

   function draw() {
      canvas.width = canvas.clientWidth;
      canvas.height = canvas.clientHeight;
      ctx.fillStyle = '#529848';
      ctx.fillRect(0, 0, canvas.width, canvas.height);

      const minWidth = 1140;
      if (canvas.width < minWidth) {
         const scale = canvas.width / minWidth;
         ctx.scale(scale, scale);
      }
      board.draw(ctx);
      requestAnimationFrame(draw);
   }
   draw();
}

board.pack.target.addEventListener('load', main);
