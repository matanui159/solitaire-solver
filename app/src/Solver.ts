import { Board } from './Board';
import { Stack } from './Stack';
import { Face } from './Card';
import { GameCard } from './GameCard';

const alternating = true;

export class Solver {
   public constructor(private board: Board) {}

   public step(): void {
      if (this.moveAnyTop()) return;
      if (this.moveAnyBottom()) return;
      this.drawCard();
   }

   private moveAnyTop(): boolean {
      for (const stack of this.board.bottom) {
         if (this.moveTop(stack)) return true;
      }
      return this.moveTop(this.board.up);
   }

   private moveTop(stack: Stack): boolean {
      if (stack.cards.length === 0) {
         return false;
      }

      const card = stack.cards[0];
      const top = this.board.top[card.suit].cards[0];
      if ((!top && card.face === Face.ace) ||
         (top && card.face === top.face + 1)) {
         stack.moveCards(1, this.board.top[card.suit]);
         return true;
      }

      return false;
   }

   private isRed(card: GameCard): boolean {
      return card.suit === 'D' || card.suit === 'H';
   }

   private getValue(card: GameCard, under?: GameCard): number {
      if ((!under && card.face === Face.king) ||
         (under && card.face === under.face - 1)) {
         if (alternating) {
            if (!under || this.isRed(card) !== this.isRed(under)) {
               return 1;
            }
         } else {
            if (!under || card.suit === under.suit) {
               return 2;
            }
            return 1;
         }
      }
      return 0;
   }

   private moveAnyBottom(): boolean {
      for (const stack of this.board.bottom) {
         if (this.moveBottom(stack)) return true;
      }
      return this.moveBottom(this.board.up, true);
   }

   private moveBottom(stack: Stack, first = false): boolean {
      for (let i = 0; i < stack.cards.length; ++i) {
         const card = stack.cards[i];
         if (!card.visible) {
            continue;
         }

         const value = this.getValue(card, stack.cards[i + 1]);
         let bestValue = 0;
         let bestStack: Stack;
         for (const bottom of this.board.bottom) {
            if (bottom === stack) {
               continue;
            }

            const v = this.getValue(card, bottom.cards[0]);
            if (v > bestValue) {
               bestValue = v;
               bestStack = bottom;
            }
         }

         if (bestValue > value) {
            stack.moveCards(i + 1, bestStack);
            return true;
         }

         if (first) {
            break;
         }
      }

      return false;
   }

   private drawCard(): void {
      const down = this.board.down;
      const up = this.board.up;
      if (down.cards.length === 0) {
         while (up.cards.length > 0) {
            const card = up.cards.shift();
            card.visible = false;
            down.cards.unshift(card);
         }
      }

      const card = down.cards.shift();
      if (card) {
         card.visible = true;
         up.cards.unshift(card);
      }
   }

   public isSolvable(): boolean {
      const solver = new Solver(this.board.clone());
      for (let i = 0; i < 1000; ++i) {
         solver.step();
      }
      return solver.board.isFinished();
   }
}
