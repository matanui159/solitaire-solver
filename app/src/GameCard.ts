import { Card, Suit, Face } from './Card';
import { newImage } from './util';

const back = newImage('back');
const animTime = 0.5;
const animGoal = 0.1;

export class GameCard implements Card {
   public readonly suit: Suit;
   public readonly face: Face;
   public readonly name: string;
   public visible = true;

   public get image(): HTMLImageElement {
      if (this.visible) {
         return this._image;
      } else {
         return back;
      }
   }

   private _image: HTMLImageElement;
   private x = -1;
   private y = -1;
   private time = -1;

   public constructor(card: Card) {
      this.suit = card.suit;
      this.face = card.face;
      this.name = card.suit + card.face;
      this._image = newImage(this.name);
   }

   public draw(ctx: CanvasRenderingContext2D, x: number, y: number): void {
      if (this.x === -1) {
         this.x = x;
         this.y = y;
         this.time = performance.now();
      }

      const now = performance.now();
      const delta = (now - this.time) / 1000;
      this.time = now;

      const dist = Math.pow(animGoal, delta / animTime);
      this.x = (this.x - x) * dist + x;
      this.y = (this.y - y) * dist + y;

      ctx.drawImage(this.image, this.x, this.y);
   }

   public clone(): GameCard {
      const card = new GameCard(this);
      card.visible = this.visible;
      card.x = this.x;
      card.y = this.y;
      card.time = this.time;
      return card;
   }
}
